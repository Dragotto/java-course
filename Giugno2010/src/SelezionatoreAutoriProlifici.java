import java.util.*;


public class SelezionatoreAutoriProlifici implements Selezionatore {

	@Override
	public List<Autore> eseguiSelezione(List<Libro> libriInBiblioteca) {
		Map<Autore,Integer> autore2num = new HashMap<Autore,Integer>();
		for(Libro l : libriInBiblioteca){
			for(Autore a : l.getAutori()){
				Integer tmp = autore2num.get(a);
				if(tmp == null){
					autore2num.put(a, 1);
				}else{
					tmp++;
					autore2num.put(a, tmp);
				}
			}
		}
		//Ho la mappa Autore -> # opere
		int max = -1;
		for(Autore a : autore2num.keySet()){
			if(autore2num.get(a) > max){
				max = autore2num.get(a);
			}
		}
		//Ho trovato il massimo
		List<Autore> ritorno = new ArrayList<Autore>();
		for(Autore a: autore2num.keySet()){
			if(autore2num.get(a) == max){
				ritorno.add(a);
			}
		}
		
		return ritorno;
	}

}
