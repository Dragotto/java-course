public class Autore {
	private int annoNascita;
	private String nome;
	public Autore(String nome, int annoNascita) {
		this.nome = nome;
		this.annoNascita = annoNascita;
	}
	public String getNome() {
		return this.nome;
	}
	public int getAnnoNascita() {
		return this.annoNascita;
	}
	
	public boolean equals(Object o){
		Autore a = (Autore)o;
		return this.nome.equals(a.getNome()) && this.annoNascita == a.getAnnoNascita();
	}
	
	public int hashCode(){
		return this.nome.hashCode() + this.annoNascita;
	}
	
}