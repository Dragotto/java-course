import static org.junit.Assert.*;

import java.util.*;

import org.junit.Before;
import org.junit.Test;


public class BibliotecaTest {

	private Biblioteca b;
	private Libro l1,l2,l3;
	private Autore a1,a2,a3;
	private SelezionatoreAutoriGiovani sGiovani;
	private SelezionatoreAutoriProlifici sProlifici;
	@Before
	public void setUp() throws Exception {
		b = new Biblioteca();
		a1 = new Autore("a1",1);
		a2 = new Autore("a2",2);
		a3 = new Autore("a3",3);
		l1 = new Libro("l1");
		l2 = new Libro("l2");
		l3 = new Libro("l3");
		l1.addAutore(a1);
		l2.addAutore(a1);
		l2.addAutore(a2);
		l3.addAutore(a3);
		
		b.addLibro("1", l1);
		b.addLibro("2", l2);
		b.addLibro("3", l3);
		sGiovani = new SelezionatoreAutoriGiovani();
		sProlifici = new  SelezionatoreAutoriProlifici();
		
	}

	@Test
	public void testAddLibro() {
		assertEquals(l1.getAutori().size(),1);
		Autore a4 = new Autore("a4",4);
		l1.addAutore(a4);
		assertEquals(l1.getAutori().size(),2);
		
	}

	@Test
	public void testAutore2libri() {
		Map<Autore,Set<Libro>> ris = b.autore2libri();
		assertEquals(ris.size(),3);
		assertTrue(ris.containsKey(a1));
		assertTrue(ris.containsKey(a2));
		assertTrue(ris.containsKey(a3));
		assertTrue(ris.get(a1).contains(l1));
		assertTrue(ris.get(a1).contains(l2));
		assertTrue(ris.get(a2).contains(l2));
		assertTrue(ris.get(a3).contains(l3));
		
	}

	@Test
	public void testSeleziona() {
		List<Autore> autoriGiovani = new ArrayList<Autore>();
		autoriGiovani = this.b.seleziona(sGiovani);
		assertTrue(autoriGiovani.contains(a3));
		assertEquals(autoriGiovani.size(),1);
		List<Autore> autoriProlifici = new ArrayList<Autore>();
		autoriProlifici = this.b.seleziona(sProlifici);
		assertTrue(autoriProlifici.contains(a1));
		assertEquals(autoriProlifici.size(),1);
	}

}