import java.util.*;
import java.util.List;


public class SelezionatoreAutoriGiovani implements Selezionatore {

	@Override
	public List<Autore> eseguiSelezione(List<Libro> libriInBiblioteca) {
		Set<Autore> a = new HashSet<Autore>();
		for(Libro l : libriInBiblioteca){
			a.addAll(l.getAutori());
		}
		Autore giovane = Collections.max(a,new ComparatoreAutoriPerEta());
		int annoGiovane = giovane.getAnnoNascita();
		List<Autore> autoriPiuGiovani = new ArrayList<Autore>();
		for(Autore autore : a){
			if(autore.getAnnoNascita() == annoGiovane){
				autoriPiuGiovani.add(autore);
			}
		}
		return autoriPiuGiovani;
	}

}
