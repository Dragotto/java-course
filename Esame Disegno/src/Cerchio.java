

public class Cerchio extends Forma {

private Punto centro;

private int diametro;

public Cerchio(Colore colore, Punto centro, int diametro) {

super(colore);
this.centro = centro;
this.diametro = diametro;
}

public Punto getCentro(){ return this.centro; }
public int getDiametro(){ return this.diametro; }

}