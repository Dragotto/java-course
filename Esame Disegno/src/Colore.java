

public class Colore {
private int red;
private int green;
private int blue;
public Colore(int red, int green, int blue) {
this.red = red; this.green = green; this.blue = blue; } 



public boolean equals(Object o){
	Colore c = (Colore) o;
	return this.red == c.getRed() && this.blue == c.getBlue() && this.green == c.getGreen();
}

public int hashCode(){
	return this.red + this.green + this.blue;
}}