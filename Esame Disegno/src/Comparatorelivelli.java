import java.util.*;
public class Comparatorelivelli implements Comparator<Forma> {

	private Disegno disegno;
	
	public Comparatorelivelli(Disegno disegno){
		this.disegno = disegno;
	}

	@Override
	public int compare(Forma f0, Forma f1) {
		return disegno.getLivello(f0) - disegno.getLivello(f1);
	}

}

