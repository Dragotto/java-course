

public class Rettangolo extends Forma {

private Punto vertice;
private int larghezza;
private int altezza;

public Rettangolo(Colore colore, Punto vertice, int altezza, int larghezza) {
super(colore);
this.vertice = vertice;
this.altezza = altezza;
this. larghezza = larghezza;
}

public Punto getVertice() { return this.vertice; }
public int getAltezza() { return this.altezza; }
public int getLarghezza() { return this.larghezza; }


}