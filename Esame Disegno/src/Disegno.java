
import java.util.*;

public class Disegno {
Map<Forma,Integer> map2livello;
public Disegno() {
this.map2livello= new HashMap<Forma,Integer>();
}

public List<Forma> formeOrdinatePerLivello() {
	List<Forma> listaFormeOrdinata = new ArrayList<Forma>(); 
	for(Forma f:map2livello.keySet()){
	listaFormeOrdinata.add(f);	
	}
    Collections.sort(listaFormeOrdinata,new Comparatorelivelli(this));
	return listaFormeOrdinata;
	}


public void aggiungiRettangolo(Rettangolo rettangolo, int livello){
map2livello.put(rettangolo, livello);
}
public void aggiungiCerchio(Cerchio cerchio, int livello){
map2livello.put(cerchio, livello);
}
public Set<Colore> coloriNelDisegno() {
Set<Colore> coloriNelDisegno = new HashSet<Colore>(); 
for(Forma f:map2livello.keySet()){
	coloriNelDisegno.add(f.getColore());
}
return coloriNelDisegno;}


public Map<Integer,List<Forma>> formeAffioranti (int livelloMinimo) {
Map<Integer,List<Forma>> livello2forme = new HashMap<Integer,List<Forma>>(); 
List<Forma> daje= new LinkedList<Forma>();
for(Forma f:map2livello.keySet()){
Integer into=map2livello.get(f);
  if(into>= livelloMinimo){
	  daje=livello2forme.get(f);
	  livello2forme.put(into,daje);}
  return livello2forme;
}

return livello2forme;
}


}