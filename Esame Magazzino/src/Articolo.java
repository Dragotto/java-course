
public interface Articolo {
	
	int getCostoUnitario();
	
	String getCodice();
    
	public boolean equals(Object o);
	
	public int hashCode();
}
