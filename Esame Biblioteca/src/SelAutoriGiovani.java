import java.util.*;


public class SelAutoriGiovani implements Selezionatore {

	public List<Autore> eseguiSelezione(List<Libro> libriInBiblioteca){
		List<Autore> autoriGiovani = new LinkedList <Autore>();
		Iterator <Libro> iteratore =libriInBiblioteca.iterator();
		
		Iterator <Autore> iteratore2 =null;
		Libro giro;
		Autore tempa=new Autore("",0);
		Autore gira;
		int anno=0;
		int annotutti=0;
		  //PRENDO L'ANNO DI NASCITA PIU'ALTO TRA TUTTI GLI AUTORI DI TUTTI I LIBRI 
		  while(iteratore.hasNext()){
		   giro=iteratore.next();
		   iteratore2=giro.getAutori().iterator();   
		       while(iteratore2.hasNext()){
		    	gira=iteratore2.next();
		    	   if(tempa.getAnnoNascita()<gira.getAnnoNascita()){
		    		tempa=gira;  
		    		anno=gira.getAnnoNascita();}}
		   if(annotutti<anno){
			   annotutti=anno;}}
	       //PRENDO OGNI AUTORE CHE HA QUELL'ANNO E LO FICCO IN AUTORIGIOVANI
		       while(iteratore.hasNext()){
		        giro=iteratore.next();
		        iteratore2=giro.getAutori().iterator();
		           while(iteratore2.hasNext()){
		           gira=iteratore2.next();
		           if(gira.getAnnoNascita()==annotutti){
		        	autoriGiovani.add(gira);}}}
		  
		       return autoriGiovani;}}
	
		   

	
	
