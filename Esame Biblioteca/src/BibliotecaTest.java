import static org.junit.Assert.*;
import static org.junit.Test.*;


import java.util.*;

import org.junit.Before;
import org.junit.Test;
public class BibliotecaTest {

   private	Biblioteca a;
private	Libro l1,l2;
private	Autore a1,a2;
private	SelezionatoreAutoriProlifici Sp;
private	SelAutoriGiovani Sg;
	
	
	@Before //CARICO LE ISTANZE
	
	public void setUp() throws Exception {
	//new biblioteca
	a=new Biblioteca();
	//new libri
	l1=new Libro("Giu");
	l2=new Libro("Down");
	//new autori
	a1=new Autore("Italo",1998);
	a2=new Autore("Smith",1997);
	//assegno ai libri gli autori
	l1.addAutore(a1);
	l2.addAutore(a2);
    //aggiungo alla biblioteca i libri
	a.addLibro("l1", l1);
    a.addLibro("l2", l2);
    //new selezionatori
	Sp=new SelezionatoreAutoriProlifici();
	Sg=new SelAutoriGiovani();}
	
	
	 //VEDO SE dopo il metodo autore2libri (CHE RITORNA MAPPA) LA MAPPA HA QUEGLI AUTORI/LIBRI
	@Test
	public void testAutore2libri() {
	Map<Autore,Set<Libro>> ris= a.autore2libri();
	assertEquals(ris.size(),2);
	assertTrue(ris.containsKey(a1));
	assertTrue(ris.containsKey(a2));
	assertTrue(ris.containsValue(l1));
	assertTrue(ris.containsValue(l2));
	}
	
	
	
}
