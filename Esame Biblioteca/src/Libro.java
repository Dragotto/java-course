import java.util.*;
//ERANO AMMMESSI DUPLICATI POICHE'MANCAVANO I METODI HASHCODE ED EQUALS
public class Libro {
private Set<Autore> autori;
private String titolo;
public Libro(String titolo) {
this.titolo = titolo;
this.autori = new HashSet<Autore>();
}
public int HashCode(){
	return this.titolo.hashCode();
}

public boolean equals(Object o){
return this.titolo.equals(o);
}

public void addAutore(Autore autore){
this.autori.add(autore);
}
public Set<Autore> getAutori() {
return Collections.unmodifiableSet(this.autori);
}
public String getTitolo(){
return this.titolo;


}
}