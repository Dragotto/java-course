import static org.junit.Assert.*;

public class GruppoDiFormeTest {

	@Test
	public void Test1(){
		GruppoDiForme G;
		G=new GruppoDiForme();
		Punto v;
		v=new Punto(1,1);
		Rettangolo r;
		r=new Rettangolo(v,5,7,"rosso");
		Cerchio c1;
		Punto centro;
		centro=new Punto(-1,-3);
		c1=new Cerchio(centro,4,"blu");
		G.AggiungiForma(r);
		G.AggiungiForma(c1);
		assertEquals(v.getX(),1);
		assertEquals(v.getY(),1);
		assertEquals(centro.getX(),-1);
		assertEquals(centro.getY(),-3);
		G.trasla(3, 2);
		assertEquals(v.getX(),4);
		assertEquals(v.getY(),3);
		assertEquals(centro.getX(),2);
		assertEquals(centro.getY(),-1);
		
	}
}
