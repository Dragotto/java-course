import static org.junit.Assert.*;

public class CerchioTest {
	
	@Test
	public void TestTrasla(){
	Cerchio c1;
	Punto centro;
	centro=new Punto(-1,-3);
	c1=new Cerchio(centro,4,"blu");
    c1.trasla(3, 2);
    assertEquals(centro.getX(),2);
    assertEquals(centro.getY(),-1);
	}
	
}
