public class Punto implements Forma {
	private int x,y;
	
	public Punto (int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void setX(int x){ 
		this.x = x; 
	}
	
	public void setY(int y){ 
		this.y = y; 
	}

	public int getX(){ 
		return this.x; 
	}
	
	public int getY(){ 
		return this.y; 
	}
	public void trasla(int x,int y){
		this.x=this.x+x;
		this.y=this.y+y;
	}
	public String toString(){
		   return "Punto coord x: "+this.x+" y: "+this.y;
	   }
}
