package it.uniroma3.diadia_0_1;

import it.uniroma3.diadia.ambienti.Labirinto;
import it.uniroma3.diadia.ambienti.Stanza;
import it.uniroma3.diadia.giocatore.Giocatore;

/**
 * Questa classe modella una partita del gioco
 *
 * @author  Paolo Merialdo, Valter Crescenzi (da un'idea di Michael Kolling and David J. Barnes)
 * @see Stanza
 * @version 0.1
 */

public class Partita {
	private Giocatore Giocatore;
	private Labirinto Labirinto;
	private boolean finita;
	
	
	public Partita(){
		creaGiocatore();
		creaLabirinto();
		this.finita = false;
		
	}

    /**
     * Crea tutte le stanze e le porte di collegamento
     */
	private void creaLabirinto(){
		this.Labirinto=new Labirinto();
	}
    private void creaGiocatore(){
    	this.Giocatore=new Giocatore();
    }
    public Giocatore getGiocatore(){
		return this.Giocatore;
	}
	
	
	
	public Labirinto getLabirinto(){
		return this.Labirinto;
	}
	/**
	 * Restituisce vero se e solo se la partita e' stata vinta
	 * @return vero se partita vinta
	 */
	public boolean vinta() {
		return this.Labirinto.getStanzaCorrente()== this.Labirinto.getStanzaVincente();
	}

	public boolean giocatoreIsVivo(){
		return (this.getGiocatore().getCfu()!=0);
	}
	
	/**
	 * Restituisce vero se e solo se la partita e' finita
	 * @return vero se partita finita
	 */
	public boolean isFinita() {
		return finita || vinta() || (this.Giocatore.getCfu() == 0);
	}

	/**
	 * Imposta la partita come finita
	 *
	 */
	public void setFinita() {
		this.finita = true;
	}

}
