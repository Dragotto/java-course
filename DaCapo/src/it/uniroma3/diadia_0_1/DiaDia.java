package it.uniroma3.diadia_0_1;

import it.uniroma3.diadia.comandi.Comandi;
import it.uniroma3.diadia.comandi.FabbricaDiComandiSemplice;

import java.util.Scanner;

/**
 *  Classe principale di diadia, un semplice gioco di ruolo ambientato al dia.
 *  Per giocare crea un'istanza di questa classe e invoca il letodo gioca
 *
 *  Questa e' la classe principale crea e istanzia tutte le altre
 *
 * @author  Paolo Merialdo (da un'idea di Michael Kolling and David J. Barnes) Modificata Barbieri Marco*
 * @version 0.1
 */

public class DiaDia {
	private Partita partita;
	private FabbricaDiComandiSemplice FabbricaDiComandi;
	private static final String MESSAGGIO_BENVENUTO = 
		"Ti trovi nell'Universita', ma oggi e' diversa dal solito...\n" +
		"Meglio andare al piu' presto in biblioteca a studiare. Ma dov'e'?\n"+
		"I locali sono popolati da strani personaggi, " +
		"alcuni amici, altri... chissa!\n"+
		"Ci sono attrezzi che potrebbero servirti nell'impresa:\n"+
		"puoi metterli nella borsa, usarli, posarli quando ti sembrano inutili\n" +
		"o regalarli se pensi che possano ingraziarti qualcuno.\n\n"+
		"Per conoscere le istruzioni usa il comando 'aiuto'.";
    


    

    public DiaDia() {
    	this.partita = new Partita();
    }

	public void gioca() {
		String istruzione; 
	    Scanner scannerDiLinee;

	    System.out.println(MESSAGGIO_BENVENUTO);
	    scannerDiLinee = new Scanner(System.in);		
		do		
			istruzione = scannerDiLinee.nextLine();
		while (!processaIstruzione(istruzione));
	}   
    
        
	/**
	 * Processa una istruzione 
	 *
	 * @return true se l'istruzione e' eseguita e il gioco continua, false altrimenti
	 */
	private boolean processaIstruzione(String istruzione) {
		      
		Comandi comandoDaEseguire=null;
		String parametro;
		Scanner scannerDiLinee;
		scannerDiLinee= new Scanner(System.in);

		comandoDaEseguire=FabbricaDiComandi.costruisciComando(istruzione);
		comandoDaEseguire.esegui(this.partita);
		if(comandoDaEseguire.getMessaggio()!=null)System.out.println(comandoDaEseguire.getMessaggio());
		//caso in cui c'� da inserire un parametro
		if(comandoDaEseguire.getErrore()!=null)
			{System.out.println(comandoDaEseguire.getErrore());
			parametro=scannerDiLinee.nextLine();
			processaIstruzione(istruzione+ " "+ parametro);
			return this.partita.isFinita();}
				
				
      	
                if (this.partita.vinta()) 
			        System.out.println("Hai vinto!");
					 
               	if(!this.partita.giocatoreIsVivo())
			        System.out.println("Hai esaurito i Cfu");
		
		        return this.partita.isFinita();
		
		} 
	
		


public static void main(String[] args) {
		DiaDia gioco = new DiaDia();
		gioco.gioca();
	}
}