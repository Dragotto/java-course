package it.uniroma3.diadia.giocatore;
import java.util.*;
import it.uniroma3.diadia.attrezzi.Attrezzo;

public class Borsa {
	
	private List<Attrezzo> attrezzi; //IMPLEMENTABILE SOLO CON LISTE O NON GETPESO
	private int pesoMax;

	
	public Borsa(){
		this.attrezzi=new ArrayList<Attrezzo>();
	}
	
		
	public boolean addAttrezzo(Attrezzo attrezzo){
		return this.attrezzi.add(attrezzo);
		
	}
	public int getPesoMax() {
		return pesoMax;
	}

	
	

    public int getPeso(){
	  int peso=0;
	  Iterator<Attrezzo> it=this.attrezzi.iterator();
	  while(it.hasNext()){
		Attrezzo a;
		a=it.next();
		peso+=a.getPeso();
	  }
	  return peso;
    }


    public Attrezzo getAttrezzo(String nomeAttrezzo){
	    Attrezzo a=null;
	    Iterator<Attrezzo> iteratore=this.attrezzi.iterator();
	    while(iteratore.hasNext()){
		   a=iteratore.next();
		   if(a.getNome().equals(nomeAttrezzo)){
			  return a;
		    }
			
	    }
        return null;
     }

	
    public boolean isEmpty(){
	   return this.attrezzi.isEmpty();}


    public boolean hasAttrezzo(String nomeAttrezzo){
	    return this.attrezzi.contains(nomeAttrezzo);
    }


   public Attrezzo removeAttrezzo(String nomeAttrezzo){
	Attrezzo a=null;
	Iterator<Attrezzo> it= this.attrezzi.iterator();
	while(it.hasNext()){
		a=it.next();
		if(a.getNome().equals(nomeAttrezzo)){
			it.remove();
			return a;
		}
	}
	return null;
}



	 
		public String toString() {
			String s = new String();
	    		if (!this.isEmpty()) {
	    			s += "Contenuto borsa ("+this.getPeso()+"kg/"+this.getPesoMax()+"kg): ";
	    			for(Attrezzo a : this.attrezzi)
	    				s += a.toString()+" ";
	    			}
	    		else 
	    			s += "Borsa vuota";
	    		return s;
		}
	 }
 