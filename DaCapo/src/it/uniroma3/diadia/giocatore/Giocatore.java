package it.uniroma3.diadia.giocatore;


public class Giocatore {
	private Borsa Borsa;
	private int cfu;
	private static int CFU_INIZIALI = 20;
	
	public Giocatore(){
	this.cfu = CFU_INIZIALI;
	}
	

	public Borsa getBorsa(){
		return this.Borsa;
	}

	public int getCfu() {
		return this.cfu;
	}

	public void setCfu(int cfu) {
		this.cfu = cfu;		
	}	
    public void menoCfu(){
    	this.cfu= this.cfu-2;
    }


}
