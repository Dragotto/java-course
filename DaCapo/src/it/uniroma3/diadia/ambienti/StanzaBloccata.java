package it.uniroma3.diadia.ambienti;
import it.uniroma3.diadia.attrezzi.Attrezzo;

public class StanzaBloccata extends Stanza{
	 
	private String direzioneBloccata;
	private String nomeAttrezzo;
	
		
	public StanzaBloccata(String nome, String direzioneBloccata,Attrezzo attrezzo){
		super(nome);
		this.direzioneBloccata = direzioneBloccata;
		this.nomeAttrezzo = attrezzo.getNome();
	}
	
	public Stanza getUscita(String direzione){
		
		if(direzione.equals(direzioneBloccata)){
			if(hasAttrezzo(nomeAttrezzo)){
				return super.getUscita(direzioneBloccata);			
			}else{
				return super.getUscita(null);
			}}else{
				return super.getUscita(direzione);
			}
	}}

    
    	
	
