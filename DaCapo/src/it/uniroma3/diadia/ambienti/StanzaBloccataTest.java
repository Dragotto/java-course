package it.uniroma3.diadia.ambienti;

import static org.junit.Assert.*;
import it.uniroma3.diadia.attrezzi.Attrezzo;

import org.junit.Before;
import org.junit.Test;

public class StanzaBloccataTest {
	
	Attrezzo pc=new Attrezzo("pc",5);
	
	StanzaBloccata cucina=new StanzaBloccata("cucina","nord",pc);
	Stanza bagno=new Stanza("bagno");
	Stanza cameradaletto=new Stanza("cameradaletto");
	Stanza soggiorno=new Stanza("soggiorno");
	Stanza atrio=new Stanza("atrio");
	String messaggio="cucina" +
					"Uscite:  sud est ovest " +
					"Attrezzi nella stanza: ";

	@Before
	public void setUp() throws Exception {
		cucina.impostaUscita("nord", bagno);
		cucina.impostaUscita("sud", cameradaletto);
		cucina.impostaUscita("est", soggiorno);
		cucina.impostaUscita("ovest", atrio);
	}


	
	
	@Test
	public void testGetUscita(){
		assertEquals(cucina.getUscita("nord"),null);
		assertEquals(cucina.getUscita("sud"),cameradaletto);
		cucina.addAttrezzo(pc);
		assertEquals(cucina.getUscita("sud"),cameradaletto);
	}

	
}
