package it.uniroma3.diadia.ambienti;

import it.uniroma3.diadia.attrezzi.Attrezzo;

class StanzaMagica extends Stanza {
    final static public int SOGLIA_DEFAULT = 3;
    private int contatoreAttrezziPosati;
    private int sogliaMagica;
	  public StanzaMagica(String nome) {
		this(nome, SOGLIA_DEFAULT);
	  }
 	  public StanzaMagica(String nome, int soglia) {
       super(nome);
       this.contatoreAttrezziPosati = 0;
       this.sogliaMagica = soglia;
	  }

 	  public void addAttrezzo(Attrezzo Attrezzo){
 	  super.addAttrezzo(Attrezzo);
 	  this.sogliaMagica=this.sogliaMagica--;
 	 contatoreAttrezziPosati=contatoreAttrezziPosati++;
 	 if(this.sogliaMagica<=0){
 		 modificaAttrezzo(Attrezzo);
 	 }
 	  }
public void modificaAttrezzo(Attrezzo Attrezzo){
	magicaStringa(Attrezzo.getNome());
	raddoppiaPeso(Attrezzo.getPeso());
}
public void raddoppiaPeso(int p){
p=p*2;
}
public void magicaStringa(String nome){
	nome=nome+" Magic";
}

}
