package it.uniroma3.diadia.ambienti;
import it.uniroma3.diadia_0_1.Partita;
public class StanzaBuia extends Stanza {
Partita partita;
	public StanzaBuia(String nome){
		super(nome);
	}
	public String getDescrizione() {
       if(partita.getGiocatore().getBorsa().hasAttrezzo("lanterna")){
		return this.toString();}
       else return "Qui c� buio pesto!";
       
	}
}
