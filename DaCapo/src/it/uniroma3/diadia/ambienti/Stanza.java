package it.uniroma3.diadia.ambienti;
import java.util.*;

import it.uniroma3.diadia.attrezzi.Attrezzo;
import it.uniroma3.diadia.personaggi.*;

/**
 * Classe Stanza - una stanza in un gioco di ruolo.
 * Una stanza e' un luogo fisico nel gioco.
 * E' collegata ad altre stanze attraverso delle uscite.
 * Ogni uscita e' associata ad una direzione.
 * @author Paolo Merialdo (a partire da un'idea di Michael Kolling e David J. Barnes)
 * @see Attrezzo
 * @version 0.1
*/

public class Stanza {
	private Personaggio personaggio;
	private String descrizione;
	private List<Attrezzo> attrezzi;
	protected Map<String,Stanza> uscite;
	

    /**
     * Crea una stanza. Inizialmente non ci sono uscite.
     * @param nome il nome della stanza
     */
    public Stanza(String descrizione) {
    	this.uscite=new HashMap<String,Stanza>();
    	this.attrezzi=new ArrayList<Attrezzo>();
    	this.descrizione=descrizione;
        
    }

    /**
     * Imposta le uscite della stanza. Ogni direzione porta ad un'altra stanza.
     *
     * @param direzione direzione in cui sara' posta la stanza.
     * @param stanza stanza da collegare alla stanza corrente tramite l'uscita
     * indicata in direzione.
     */
    public void impostaUscita(String direzione, Stanza stanza) {
       this.uscite.put(direzione, stanza);
    }

    
    public void setPersonaggio(Personaggio personaggio) {
		this.personaggio = personaggio;	
	}
    public Personaggio getPersonaggio() {
		return this.personaggio;	
	}

    /**
     * Restituisce la stanza dell'uscita specificata
     * @param direzione
     */
	public Stanza getUscita(String direzione) {
       
		 return this.uscite.get(direzione);
	}

    /**
     * Restituisce la descrizione della stanza.
     * @return descrizione della stanza
     */
    public String getDescrizione() {
        return this.descrizione;
    }
	
     /**
     * Colloca un attrezzo nella stanza.
     * @param attrezzo l'attrezzo da collocare nella stanza.
     *        Impostare a null per rappresentare l'assenza
     *        di attrezzi nella stanza.
     */
    public void addAttrezzo(Attrezzo attrezzo) {
    	this.attrezzi.add(attrezzo);
    }

    
    /**
	* Controlla se un attrezzo esiste nella stanza (uguaglianza sul nome).
	* @return true se l'attrezzo esiste nella stanza, false altrimenti.
	*/
	public boolean hasAttrezzo(String nomeAttrezzo) {
		
		return this.attrezzi.contains(nomeAttrezzo);
	}

	/**
     * Restituisce l'attrezzo nomeAttrezzo se presente nella stanza.
	 * @param nomeAttrezzo
	 * @return l'attrezzo presente nella stanza.
     * 		   null se l'attrezzo non e' presente.
	 */
    public Attrezzo getAttrezzo(String nomeAttrezzo) {
		
	     Attrezzo a=null;
	     Iterator<Attrezzo> iteratore=this.attrezzi.iterator();
	     while(iteratore.hasNext()){
		   a=iteratore.next();
		   if(a.getNome().equals(nomeAttrezzo)){
		   return a;
		   }
         }
         return null;
	}

	/**
	 * Rimuove un attrezzo dalla stanza (ricerca in base al nome).
	 * @param nomeAttrezzo
	 */
	public boolean removeAttrezzo(Attrezzo attrezzo) {
		boolean trovato=false;
		Attrezzo a=null;
		Iterator<Attrezzo> it= this.attrezzi.iterator();
		while(it.hasNext()){
			a=it.next();
			if(a.getNome().equals(attrezzo)){
				it.remove();
				trovato=true;}}
		
		 return trovato;
	}
	
	 /**
	* Restituisce una rappresentazione stringa di questa stanza,
	* stampadone la descrizione, le uscite e gli eventuali attrezzi contenuti
	* @return la rappresentazione stringa
	*/
    public String toString() {
    	
    	String s = new String();
    	s += this.descrizione;
    	
    	s += "\nAttrezzi nella stanza: ";
    	
    	
    	for(Attrezzo a : this.attrezzi)
			s += a.toString()+" ";
		
	 
    	
    	return s;
    	
    }
//
public Stanza vicinameno(){
Stanza a;
a=new Stanza("IMPLEMENTA METODO RITORNA STANZA CON MENO OGGETTI");
return a;
}
    
public Stanza vicinapiu(){
	Stanza a;
	a=new Stanza("IMPLEMENTA METODO RITORNA STANZA CON piu' OGGETTI");
	return a;
}
    
    
}