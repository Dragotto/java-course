package it.uniroma3.diadia.ambienti;

import it.uniroma3.diadia.attrezzi.Attrezzo;

public class Labirinto {
	private Stanza stanzaCorrente;
	private Stanza stanzaVincente;
	
	public Labirinto(){
	creaStanze();
	}
	
	
	
	private void creaStanze() {

		/* crea gli attrezzi */
    	Attrezzo lanterna = new Attrezzo("lanterna",3);
		Attrezzo osso = new Attrezzo("osso",1);
    	
		/* crea stanze del labirinto */
		Stanza atrio = new Stanza("Atrio");
		Stanza aulaN11 = new Stanza("Aula N11");
		Stanza aulaN10 = new Stanza("Aula N10");
		Stanza laboratorio = new Stanza("Laboratorio Campus");
		Stanza biblioteca = new Stanza("Biblioteca");
		
		/* collega le stanze */
		atrio.impostaUscita("nord", biblioteca);
		atrio.impostaUscita("est", aulaN11);
		atrio.impostaUscita("sud", aulaN10);
		atrio.impostaUscita("ovest", laboratorio);
		aulaN11.impostaUscita("est", laboratorio);
		aulaN11.impostaUscita("ovest", atrio);
		aulaN10.impostaUscita("nord", atrio);
		aulaN10.impostaUscita("est", aulaN11);
		aulaN10.impostaUscita("ovest", laboratorio);
		laboratorio.impostaUscita("est", atrio);
		laboratorio.impostaUscita("ovest", aulaN11);
		biblioteca.impostaUscita("sud", atrio);

        /* pone gli attrezzi nelle stanze */
		aulaN10.addAttrezzo(lanterna);
		atrio.addAttrezzo(osso);

		// il gioco comincia nell'atrio
        stanzaCorrente = atrio;  
		stanzaVincente = biblioteca;
    }

	public Stanza getStanzaVincente() {
		return stanzaVincente;
	}

	public void setStanzaCorrente(Stanza stanzaCorrente) {
		this.stanzaCorrente = stanzaCorrente;
	}

	public Stanza getStanzaCorrente() {
		return this.stanzaCorrente;
	}
	
}
