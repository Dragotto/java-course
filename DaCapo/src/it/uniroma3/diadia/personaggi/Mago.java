package it.uniroma3.diadia.personaggi;
import it.uniroma3.diadia.attrezzi.*;
import it.uniroma3.diadia_0_1.*;
public class Mago extends Personaggio {
	private static final String MESSAGGIO_DONO = "Sei un vero simpaticone, " +
		"con una mia magica azione, troverai un nuovo oggetto " +
		"per il tuo bel borsone!";
	private static final String MESSAGGIO_SCUSE = "Mi spiace, ma non ho piu' nulla da darti ...";
	private static final String Messaggio_ok = "Et voil�,peso dimezzato!";
	private Attrezzo attrezzo;

	public Mago(String nome, String presentazione, Attrezzo attrezzo) {
		super(nome, presentazione);
		this.attrezzo = attrezzo;
	}

	public String agisci(Partita partita) {
		String msg;
		if (attrezzo!=null) {
			partita.getLabirinto().getStanzaCorrente().addAttrezzo(attrezzo);
			this.attrezzo = null;
			msg = MESSAGGIO_DONO;
		}
		else {
			msg = MESSAGGIO_SCUSE;
		}
		return msg;
	}
	public String riceviRegalo(Attrezzo attrezzo, Partita partita) {
	Attrezzo a;
	a=new Attrezzo("",0);
	a=attrezzo;
	a.setPeso();
	partita.getLabirinto().getStanzaCorrente().addAttrezzo(a);
	return Messaggio_ok;
	}
}
