package it.uniroma3.diadia.personaggi;
	import it.uniroma3.diadia.attrezzi.*;
	import it.uniroma3.diadia_0_1.*;
	public class Strega extends Personaggio {
		private static final String MESSAGGIO_ride ="wahhahahaha! (d� addio al tuo attrezzo)";
		public Strega(String nome, String presentazione, Attrezzo attrezzo) {
			super(nome, presentazione);
			
		}

		public String agisci(Partita partita) {
		String risposta;
		if (this.haSalutato()){
			risposta="Non mi hai salutato..ti invier� nella stanza con meno attrezzi";
		partita.getLabirinto().setStanzaCorrente(partita.getLabirinto().getStanzaCorrente().vicinameno());
		}
		else
			risposta="tu mi hai salutato..ti invier� nella stanza con piu attrezzi";
		partita.getLabirinto().setStanzaCorrente(partita.getLabirinto().getStanzaCorrente().vicinapiu());
			
		return risposta;
		}
		
		public String riceviRegalo(Attrezzo attrezzo, Partita partita) {
			return MESSAGGIO_ride;
			}
		}
	

