package it.uniroma3.diadia.personaggi;
import it.uniroma3.diadia.attrezzi.*;
import it.uniroma3.diadia_0_1.*;
public class Cane extends Personaggio {
	private static final String Preferito="osso";
	private static final String MESSAGGIO_morso = "...rrr " +
		"woof woof woof " +
		"arf! (morde)..Hai perso 2 Cfu";
	private static final String MESSAGGIO_osso ="Bau Bau....tonf!";
	private static final String MESSAGGIO_no ="Bau Bau....tonf!";
	public Cane(String nome, String presentazione, Attrezzo attrezzo) {
		super(nome, presentazione);
		
	}

	public String agisci(Partita partita) {
		String morso;
		morso=MESSAGGIO_morso;
		partita.getGiocatore().menoCfu();
		return morso;
	}
	public String riceviRegalo(Attrezzo attrezzo, Partita partita) {
		Attrezzo a;
		a=new Attrezzo("collare",2);
		if(attrezzo.equals(Preferito)){
			partita.getLabirinto().getStanzaCorrente().addAttrezzo(a);
			return MESSAGGIO_osso;}
		else{
			a=attrezzo;
		partita.getLabirinto().getStanzaCorrente().addAttrezzo(a);
		return MESSAGGIO_no;
		}
	}
}
