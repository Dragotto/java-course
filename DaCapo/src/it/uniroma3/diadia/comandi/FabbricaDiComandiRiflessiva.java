package it.uniroma3.diadia.comandi;


import java.util.Scanner;
public class FabbricaDiComandiRiflessiva implements FabbricadiComandi {

	public Comandi costruisciComando(String istruzione) {
		Scanner scannerDiParole = new Scanner(istruzione);
		String nomeComando = null; 
		String parametro = null;
    	Comandi comando = null;
    
		if (scannerDiParole.hasNext())
    		nomeComando = scannerDiParole.next(); // prima parola: nome del comando
    	if (scannerDiParole.hasNext())
    		parametro = scannerDiParole.next();// seconda parola: eventuale parametro
    	try {
    		String nomeClasse = "it.uniroma3.diadia.comandi.Comando";
    		nomeClasse += Character.toUpperCase(nomeComando.charAt(0));
    		nomeClasse += nomeComando.substring(1);
    		comando = (Comandi)Class.forName(nomeClasse).newInstance();
    		comando.setParametro(parametro);
    	} catch (Exception e) { 
    		comando = new ComandoNonValido();
    		comando.setParametro("Comando inesistente");
    	}
		return comando;
	}  
}
