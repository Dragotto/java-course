package it.uniroma3.diadia.comandi;

import java.util.Scanner;

public class FabbricaDiComandiSemplice  {
	public static Comandi costruisciComando(String istruzione){
		Scanner scannerDiParole=new Scanner(istruzione);
		String nomeComando=null;
		String nomeParametro=null;
		Comandi comando=null;
		
		if(scannerDiParole.hasNext()){
			nomeComando=scannerDiParole.next();
		}
		if(scannerDiParole.hasNext()){
			nomeParametro=scannerDiParole.next();
		}
		
		if(nomeComando==null){
			comando= new ComandoNonValido();
			return comando;
		}
		
		if(nomeComando.equals("vai")){
			comando=new ComandoVai(null);
			comando.setParametro(nomeParametro);}
		
		if(nomeComando.equals("prendi")){
			comando=new ComandoPrendi();
			comando.setParametro(nomeParametro);}
		
		if(nomeComando.equals("aiuto")){
			comando=new ComandoAiuto();
			comando.setParametro(nomeParametro);}
		
		if(nomeComando.equals("posa")){
			comando=new ComandoPosa();
			comando.setParametro(nomeParametro);}
		
		if(nomeComando.equals("vediborsa")){
			comando=new ComandoVediBorsa();
			comando.setParametro(nomeParametro);}
		
		if(nomeComando.equals("vedistanza")){
			comando=new ComandoVediStanza();
			comando.setParametro(nomeParametro);}
		
		if(nomeComando.equals("fine")){
			comando=new ComandoFine();
			comando.setParametro(nomeParametro);}
		if(nomeComando.equals("regala")){
			comando=new ComandoRegala();
			comando.setParametro(nomeParametro);}
		if(nomeComando.equals("saluta")){
			comando=new ComandoSaluta();
			comando.setParametro(nomeParametro);}
		if(nomeComando.equals("interagisci")){
			comando=new ComandoInteragisci();
			comando.setParametro(nomeParametro);}
	if(comando==null)
		comando=new ComandoNonValido();
		return comando;
	
}
	
}
