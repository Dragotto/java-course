package it.uniroma3.diadia.comandi;

import it.uniroma3.diadia_0_1.Partita;

public interface Comandi {
	void esegui (Partita partita);
	void setParametro(String parametro);
    public String getErrore();
	public String getMessaggio();

}
