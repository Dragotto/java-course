package it.uniroma3.diadia.comandi;

import it.uniroma3.diadia_0_1.Partita;

public class ComandoPrendi implements Comandi {
private String nomeAttrezzo;
private String errore=null;
private String messaggio=null;
public String getErrore(){
	return this.errore;
}

public String getMessaggio(){
	return this.messaggio;
}
public void esegui (Partita partita){
	if(nomeAttrezzo==null)
		System.out.println("dimmi anche nome dell attrezzo!");
	else if(!(partita.getLabirinto().getStanzaCorrente().hasAttrezzo(nomeAttrezzo)))
    System.out.println("L Attrezzo non � in questa stanza!");
	else {if(partita.getGiocatore().getBorsa().getPesoMax()>=(partita.getGiocatore().getBorsa().getPeso()+partita.getLabirinto().getStanzaCorrente().getAttrezzo(nomeAttrezzo).getPeso())){
		partita.getGiocatore().getBorsa().addAttrezzo(partita.getLabirinto().getStanzaCorrente().getAttrezzo(nomeAttrezzo));
	    partita.getLabirinto().getStanzaCorrente().removeAttrezzo(partita.getLabirinto().getStanzaCorrente().getAttrezzo(nomeAttrezzo));
	    System.out.print("Hai Preso: "+partita.getGiocatore().getBorsa().getAttrezzo(nomeAttrezzo));
	}else System.out.print("Lo spazio nella Borsa non � sufficiente!");
	
	}}
public void setParametro(String parametro){
	this.nomeAttrezzo=parametro;
}


}
