package it.uniroma3.diadia.comandi;

import it.uniroma3.diadia_0_1.Partita;

public class ComandoPosa implements Comandi {
private String nomeAttrezzo;
private String errore=null;
private String messaggio=null;
public String getErrore(){
	return this.errore;
}

public String getMessaggio(){
	return this.messaggio;
}


public void esegui (Partita partita){
	if(nomeAttrezzo==null)
		System.out.println("dimmi anche nome dell attrezzo!");
	else if(!(partita.getGiocatore().getBorsa().hasAttrezzo(nomeAttrezzo)))
			System.out.println("L Attrezzo non � in Borsa,sciocco!");
	else {partita.getLabirinto().getStanzaCorrente().addAttrezzo(partita.getGiocatore().getBorsa().getAttrezzo(nomeAttrezzo));
          partita.getGiocatore().getBorsa().removeAttrezzo(nomeAttrezzo);
          System.out.println("Hai posato: "+partita.getLabirinto().getStanzaCorrente().getAttrezzo(nomeAttrezzo).getNome());
	}
}
public void setParametro(String parametro){
	this.nomeAttrezzo=parametro;
}


}
