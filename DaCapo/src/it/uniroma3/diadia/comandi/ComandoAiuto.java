package it.uniroma3.diadia.comandi;

import it.uniroma3.diadia_0_1.Partita;

public class ComandoAiuto implements Comandi {
    private static String[] elencoComandi = {"vai", "aiuto", "fine","prendi","posa","vedistanza","vediborsa","interagisci","saluta","regala"};
    private String errore=null;
	private String messaggio=null;
	
public void esegui(Partita partita){
	for(int i=0; i< elencoComandi.length; i++) 
		System.out.print(elencoComandi[i]+" ");
	System.out.println();
}
public void setParametro(String parametro){}
public String getErrore(){
	return this.errore;
}

public String getMessaggio(){
	return this.messaggio;
}

}
