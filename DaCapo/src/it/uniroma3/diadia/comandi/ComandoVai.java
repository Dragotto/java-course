package it.uniroma3.diadia.comandi;

import it.uniroma3.diadia.ambienti.Stanza;
import it.uniroma3.diadia_0_1.Partita;

public class ComandoVai implements Comandi {
	private String direzione;
	private String errore=null;
	private String messaggio=null;
	
	public ComandoVai(String direzione){
		this.direzione=direzione;
	}
	 public String getErrore(){
	    	return this.errore;
	    }
		
		public String getMessaggio(){
			return this.messaggio;
		}
	public void esegui(Partita partita){
		 if(direzione==null)
			 System.out.println("Dove vuoi andare ?");
		 Stanza prossimaStanza = null;
		 prossimaStanza = partita.getLabirinto().getStanzaCorrente().getUscita(direzione);
		 if (prossimaStanza == null)
			 System.out.println("Direzione inesistente");
		 else {
			partita.getLabirinto().setStanzaCorrente(prossimaStanza);
			int cfu = partita.getGiocatore().getCfu();
			partita.getGiocatore().setCfu(cfu-1);
		 }
		System.out.println(partita.getLabirinto().getStanzaCorrente().getDescrizione());
	}

	public void setParametro(String parametro){
	this.direzione=parametro;

	}
}
