package it.uniroma3.diadia.comandi;
import it.uniroma3.diadia_0_1.Partita;
import it.uniroma3.diadia.personaggi.*;
public class ComandoInteragisci implements Comandi {
	private String messaggio;

	public void esegui(Partita partita) {
		Personaggio personaggio;
		personaggio = partita.getLabirinto().getStanzaCorrente().getPersonaggio();
		this.messaggio = personaggio.agisci(partita);
		return;
	}

	public String getErrore() {
		return null;
	}

	public String getMessaggio() {
		return this.messaggio;
	}

	public void setParametro(String parametro) {
	}
}
