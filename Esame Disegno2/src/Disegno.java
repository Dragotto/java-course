
import java.util.*;
public class Disegno {
private Map<Integer,List<Forma>> livello2forme;
public Disegno() {
this.livello2forme= new HashMap <Integer,List<Forma>>();
}

public List<Forma> formeOrdinatePerLuminosita() {
	List<Forma> forme = null;
	List<Forma> tmp = null;
	for(Integer I:livello2forme.keySet()){
    	tmp=livello2forme.get(I);
    }for(Forma f:tmp){
    	forme.add(f);
    }
    Collections.sort(forme,new ComparatoreLuminositÓ());
	return forme;
	}


public Map<Colore, List<Forma>> colore2forme() {
	Map<Colore, List<Forma>> colore2forme = null;
	List<Forma> forme=null;
    for(Integer i:livello2forme.keySet()){
    	forme=livello2forme.get(i);
    	for(Forma f:forme){
    		colore2forme.put(f.getColore(), forme);
    	}
    }
    Collections.sort(colore2forme,new ComparatoreColori());
	return colore2forme;
	}





public void aggiungiForma(Forma forma, int livello){
List<Forma> n=new LinkedList<Forma>();
for(Integer lvcorrente:livello2forme.keySet()){
	if(lvcorrente==livello){
		n.add(forma);
		livello2forme.put(lvcorrente,n);
	}else{
	n.add(forma);
	livello2forme.put(livello,n);}}

}


public Set<Colore> coloriPresentiNelDisegno() {
Set<Colore> coloriNelDisegno = null;
List<Forma> formelv= new LinkedList<Forma>();
for(Integer i:livello2forme.keySet()){
	formelv= livello2forme.get(i);
	for(Forma f:formelv){
	coloriNelDisegno.add(f.getColore());
	}

}return coloriNelDisegno;}





}