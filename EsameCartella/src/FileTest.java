import static org.junit.Assert.*;

import java.util.Map;
import java.util.Set;

public class FileTest {
private File uno,due,tre;
private Cartella car,car2;

@Before
   public void setUp(){
	uno=new File("pippo",1,"p");
	due=new File("raschio",2,"r");
	tre=new File("mimo",5,"p");
	
	car=new Cartella("cara","p");
	car2=new Cartella("lara","r");
    car.addRisorse(due);
	car.addRisorse(uno);
    car.addRisorse(tre);
    car2.addRisorse(due);
    car2.addRisorse(uno);
    car2.addRisorse(tre);
}

@Test
public void testproprietario2risorsa(){
	Map<String, Set<Risorsa>> mappa= car2.proprietario2risorsa();
	assertEquals(mappa.size(),1);
	assertTrue(mappa.containsKey(due));
    assertFalse(mappa.containsKey(tre));
    
}

@Test
public void testproprietario2risorsa(){
	Map<String, Set<Risorsa>> mappa= car.proprietario2risorsa();
	assertEquals(mappa.size(),1);
	assertTrue(mappa.containsKey(uno));
}


}
