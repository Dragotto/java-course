import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class File extends Risorsa{
	private int dimensione;

	public File(String nome, int dimensione, String proprietario) {
		super(nome,proprietario);
		this.dimensione = dimensione;
	}
	
	public int hashCode() { return this.getNome().hashCode(); }
	
	public boolean equals(Object obj) {
		File file = (File)obj;
		return this.getNome().equals(file.getNome());
	}

	@Override
	public int getDimensione() {
		return this.dimensione;
	}

	@Override
	public Map<String, Set<Risorsa>> proprietario2risorsa() {
		Map<String,Set<Risorsa>> m = new HashMap<String,Set<Risorsa>>();
		Set<Risorsa> v = new HashSet<Risorsa>();
		v.add(this);
		m.put(this.getProprietario(), v);
		return m;
	}
	
	@Override
	public int compareTo(Risorsa arg0) {
		return this.getNome().compareTo(arg0.getNome());
	}
}