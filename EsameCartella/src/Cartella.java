import java.util.*;
public class Cartella extends Risorsa{
	private Set<Risorsa> elencoRisorse;

	public Cartella(String nome, String proprietario) {
		super(nome,proprietario);
		this.elencoRisorse = new TreeSet<Risorsa>();
	}
	
	public void addRisorse(Risorsa r) { this.elencoRisorse.add(r); }
	
	public int getDimensione() {
		int dimensione = 0;
		for(Risorsa r : this.elencoRisorse)
			dimensione += r.getDimensione();
		return dimensione;
	}
	
	public int hashCode() { return this.getNome().hashCode(); }
	
	public boolean equals(Object obj) {
		Cartella cartella = (Cartella)obj;
		return this.getNome().equals(cartella.getNome());
	}
	
	public Map<String, Set<Risorsa>> proprietario2file(){
		Map<String, Set<Risorsa>> mappa = new TreeMap<String, Set<Risorsa>>();
		for(Risorsa r : this.elencoRisorse){
			if(mappa.containsKey(r.getProprietario())){
				mappa.get(r.getProprietario()).add(r);
			}else{
				Set<Risorsa> lista = new TreeSet<Risorsa>();
				lista.add(r);
				mappa.put(r.getProprietario(), lista);
			}
			Map<String,Set<Risorsa>> app = r.proprietario2risorsa();
			for(String s: app.keySet()){
				if(mappa.containsKey(s)){
					mappa.get(s).addAll(app.get(s));
				}else{
					mappa.put(s, app.get(s));
				}
			}
		}
		return mappa;
	}

	@Override
	public Map<String, Set<Risorsa>> proprietario2risorsa() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int compareTo(Risorsa arg0) {
		return this.getNome().compareTo(arg0.getNome());
	}
}