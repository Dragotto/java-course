import java.util.*;
public class Consorzio extends Societ� {
private int annoDiCostituzione;
private String nazione;
private String nome;
private Map<String, Societ�> Societ�;

public Consorzio(int annoDiCostituzione, String nazione, String nome) {
super(annoDiCostituzione,nazione,nome);
this.Societ� = new HashMap<String,Societ�>();
}

public void stampaelenco(List<Societ�> l){
	for(Societ� s:l){
	System.out.print(s.getAnnoDiCostituzione());
	System.out.print(s.getNazione());
	System.out.print(s.getNome());
	}
	}

public int getAnnoDiCostituzione(){ return this.annoDiCostituzione;}

public String getNazione() { return this.nazione; }

public String getNome() { return this.nome; }

public void aggiungiConsorziata(Societ� societa) {
	this.Societ�.put(societa.getNome(), societa);
}

public List<Societ�> societaConsorziate(){
	List<Societ�> values = (LinkedList<Societ�>)this.Societ�.values();
	return values;
}


public int getNumeroDipendenti () {
int numeroproprio=0;
List<Societ�> societaConsorziate = this.societaConsorziate();
for(Societ� i: societaConsorziate){
	numeroproprio=numeroproprio+i.getNumeroDipendenti();
}
     return numeroproprio+this.getNumeroDipendenti();
}

public Map<String, List<Societ�>> nazione2consorziate() {
	Map<String, List<Societ�>> risultato = 
		new HashMap<String,List<Societ�>>();
	List<Societ�> societaConsorziate = this.societaConsorziate();
	
	List<Societ�> valore = new LinkedList<Societ�>();
	valore.add(this);
	risultato.put(this.getNazione(), valore);
	
	for(Societ� i: societaConsorziate){
		Set<String> nazioniDiI = (HashSet<String>)i.nazione2consorziate().keySet();
		for(String s: nazioniDiI){
			if(risultato.containsKey(s)){
				risultato.get(s).addAll(i.nazione2consorziate().get(s));
			}
			else{
				risultato.put(s, i.nazione2consorziate().get(s));
			}
		}
	}
	for(String i: risultato.keySet()){
		Collections.sort(risultato.get(i),new ComparatoreSocietaPerAnno());
	}
	return risultato;
}

}



