import junit.*;
import static org.junit.Assert.*;
import static org.junit.Test.*;

public class ConsorzioTest {
   private Consorzio nullo;
   private Consorzio stessanazione;
   private Consorzio altranazione;
   private Impresa uno;
   private Impresa duenaz;
   private Impresa duenaz2;
   private Impresa duenon;
   private Impresa duenon2;
   private Dipendente giacomo;
   private Dipendente giacomo2;
   private Dipendente giacomo3;
   private Dipendente giacomo4;
   private Dipendente gianni;
   private Dipendente gianni2;
   private Dipendente gianni3;
   private Dipendente gianni4;
   @Before
   public void setUp(){
	   nullo=null;
	   uno=new Impresa(1999,"Francia","F");
	   duenaz=new Impresa(1999,"Italia","I");
	   duenaz2=new Impresa(1999,"Italia","I2");
	   duenon=new Impresa(1999,"Germania","G");
	   duenon=new Impresa(1999,"UK","UK");
	   stessanazione.aggiungiConsorziata(duenaz);
	   stessanazione.aggiungiConsorziata(duenaz2);
	   altranazione.aggiungiConsorziata(duenon);
	   altranazione.aggiungiConsorziata(duenon2);
       giacomo=new Dipendente();
       giacomo2=new Dipendente();
       giacomo3=new Dipendente();
       giacomo4=new Dipendente();
       gianni=   new Dipendente();
       gianni2=   new Dipendente();
       gianni3=   new Dipendente();
       gianni4=   new Dipendente();
       uno.aggiungiDipendente(giacomo);
       duenaz.aggiungiDipendente(giacomo2);
       duenaz.aggiungiDipendente(giacomo3);
       duenon.aggiungiDipendente(gianni);
       duenon.aggiungiDipendente(gianni2);
   }
       
   @Test
   public void GetnumDipendentiTestnull(){
   assertEquals(nullo.getNumeroDipendenti(),0);
   }
	
   @Test
   public void GetnumDipendentiTestuna(){
   assertEquals(uno.getNumeroDipendenti(),1);
   }
   @Test
   public void GetnumDipendentiTeststessaNazione(){
   assertEquals(stessanazione.getNumeroDipendenti(),2);
   }
   @Test
   public void GetnumDipendentiTestaltraNazione(){
   assertEquals(altranazione.getNumeroDipendenti(),2);
   }


}
