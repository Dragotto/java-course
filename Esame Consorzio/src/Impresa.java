import java.util.*;
public class Impresa extends Societ� {
private int annoDiCostituzione;
private String nazione;
private String nome;
private Set<Dipendente> dipendenti;

public Impresa(int annoDiCostituzione, String nazione, String nome) {
super(annoDiCostituzione,nazione,nome);
this.dipendenti = new HashSet<Dipendente>();
}
public int getAnnoDiCostituzione() { return this.annoDiCostituzione; }


public String getNazione() { return this.nazione; }


public void aggiungiDipendente(Dipendente dipendente) {
this.dipendenti.add(dipendente); }


public String getNome() { return this.nome; }


public int getNumeroDipendenti() {
return this.dipendenti.size();
}

public Societ� getthisImpresa(){
	return(this);
}

public Map<String, List<Societ�>> nazione2consorziate(){
	Map<String, List<Societ�>> ritorna= new HashMap<String, List<Societ�>>();
	List<Societ�> valore = new LinkedList<Societ�>();
	valore.add(getthisImpresa());
	ritorna.put(this.getNazione(), valore);
	return ritorna;
}
}


