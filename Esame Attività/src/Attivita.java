import java.util.*;
public class Attivita extends Compito {
private String nome;
private int costo;
private String responsabile;

public Attivita(String nome, String responsabile, int costo) {
super(nome,responsabile);
this.costo = costo;
}
public Map<String, List<Compito>> responsabile2compito(){
Map<String,List<Compito>> attivita=new HashMap<String,List<Compito>>();
List<Compito> lista=new LinkedList<Compito> ();
lista.add(this);
attivita.put(this.responsabile,lista);

return attivita;
}



public int getCosto() { return this.costo; }

public String getNome() { return this.nome; }

public String getResponsabile() { return this.responsabile; }

public void setResponsabile(String responsabile) {this.responsabile = responsabile; }

public int hashCode() { return this.getNome().hashCode(); }

public boolean equals(Object obj) {
Attivita attivita = (Attivita)obj;
return this.getNome().equals(attivita.getNome());
}

}