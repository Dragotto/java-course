
import java.util.*;

public class Progetto extends Compito {
private List<Compito> elencocompiti;
private String nome;
private String responsabile;

public Progetto(String nome, String responsabile) {
super(nome,responsabile);
this.elencocompiti = new ArrayList<Compito>();
}
public Map<String, List<Compito>> responsabile2compito(){
	Map<String,List<Compito>> attivita=new HashMap<String,List<Compito>>();
	List<Compito> listarespcorr= new LinkedList<Compito>();
	for(Compito c:elencocompiti){
	listarespcorr.add(c);
	attivita.put(c.getResponsabile(),listarespcorr);
	}

	return attivita;
	}



public void addCompito(Compito compito) { this.elencocompiti.add(compito);
Collections.sort(this.elencocompiti,new ComparatoreOrdine());}

public String getNome() { return this.nome; }

public String getResponsabile() { return this.responsabile; }

public void setResponsabile(String responsabile) {this.responsabile = responsabile; }

public int getCosto() {
int costo = 0;
for(Compito c : this.elencocompiti)
costo += c.getCosto();
return costo;
}
public int hashCode() { return this.getNome().hashCode(); }

public boolean equals(Object obj) {
Progetto progetto = (Progetto)obj;
return this.getNome().equals(progetto.getNome());
}


}