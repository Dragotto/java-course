import org.junit.*;
import org.junit.Assert;
public class ProgettoTest {
private Attivita aa,aa2,a,a2;
private Progetto solo,fuori,dentro;


@Before
public void setUp(){
aa=new Attivita("tz","t",3);	
aa2=new Attivita("tza","t",9);
a=new Attivita("sola","t",6);
a2=new Attivita("dentro","t",8);
solo.addCompito(aa);
solo.addCompito(aa2);
fuori.addCompito(dentro);
dentro.addCompito(a2);
fuori.addCompito(a);
}
@Test
public void getCostoprogsTest(){
assertEquals(solo.getCosto(),12);
}

@Test
public void getCostopropTest(){
assertEquals(fuori.getCosto(),14);
assertEquals(dentro.getCosto(),8);
}

}
