import java.util.*;
public class ElencoPersone3 {
    private List<Persona> elenco;
    
    public ElencoPersone3(){
        this.elenco = new ArrayList<Persona>();
    }

    public void aggiungiPersona(Persona persona) {
        this.elenco.add(persona);
    }

    //RITORNA MAPPA CON CHIAVE ETA' PER VALORE LISTA DELLE PERSONE con ETA' = CHIAVE
    public Map<Integer, List<Persona>> eta2persona() {
        Integer c=3;
    	Map<Integer, List<Persona>> eta2persona = new HashMap<Integer, List<Persona>>(); 
        List<Persona> b;
        b =new LinkedList<Persona>();
    	Iterator<Persona> p= elenco.iterator();
        while (p.hasNext()){
        	Persona a;
        	a=p.next();
        	if(a.getEta()==3){
        	b.add(a);	
        	}
        }
        eta2persona.put(c,b);
        return eta2persona;
    }
}