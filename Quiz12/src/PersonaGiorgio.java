
public class PersonaGiorgio {
	    private int eta;
	    private String nome;

	    public PersonaGiorgio(String nome, int eta) {
	        this.nome = nome;
	        this.eta = eta;
	    }

	    public void cambioNome(String nome){
	        this.nome = nome;
	    }

	    public String toString() {
	        return this.nome + " " + this.eta;
	    }

	    public static void main(String[] args) {
	    	PersonaGiorgio p1 = new PersonaGiorgio("Marco",40);
	    	PersonaGiorgio p2 = new PersonaGiorgio("Luca", 20);

	        System.out.println(p1.toString());
	        System.out.println(p2.toString());

	        p2 = p1;
	        p2.cambioNome("Giorgio");

	        System.out.println(p1.toString());
	        System.out.println(p2.toString());
	    }
	}

