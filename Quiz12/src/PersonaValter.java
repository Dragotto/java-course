
public class PersonaValter {
	private String nome;
    private int eta;

    public PersonaValter(String nome, int eta) {
        this.nome = nome;
        this.eta = eta;
    }

    public void cambioNome(String nome){
        this.nome = nome;
    }

    public String toString() {
        return this.nome + " " + this.eta;
    }

    public static void main(String[] args) {
    	PersonaValter paolo = new PersonaValter("Paolo", 40);
    	PersonaValter valter = new PersonaValter("Valter",30);

        System.out.println(paolo);
        System.out.println(valter);
        paolo = valter;

        paolo.cambioNome("Luigi");
        System.out.println(paolo);
        System.out.println(valter);
    }
}

