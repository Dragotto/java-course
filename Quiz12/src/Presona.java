public class Presona {
    private String nome;

    public Presona(String nome) {
        this.nome = nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return this.nome;
    }


    public static void main(String args[]) {
        Presona[] aula = new Presona[10];
        Presona p1 = new Presona("paolo");

        aula[0] = p1;
        p1.setNome("ambrogio");
        aula[1] = p1;
        aula[2] = new Presona("luigi");
        aula[3] = new Presona("anna");

        Presona docente = new Presona("anna");

        for(int i = 0; i<4; i++)
            if (aula[i] != docente)
                System.out.println(aula[i].getNome()+" ");
     }
}

