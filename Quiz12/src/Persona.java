import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

class Persona implements Comparable<Persona> {
    private String cognome;
    private String nome;
    private int eta;
    
    public Persona(String cognome, String nome, int eta) {
        this.cognome = cognome;
        this.nome = nome;
        this.eta = eta;
    }

    public int getEta() {
        return this.eta;
    }
    
    public String getNome() {
        return this.nome;
    }
    
    public String getCognome() {
        return this.cognome;
    }
    
    public int compareTo(Persona persona) {
        return this.getEta() - persona.getEta();
    }    
}

class ComparaPersonePerCognomeNome implements Comparator<Persona> {

    public int compare(Persona p1, Persona p2) {
    String Nome;
    String Cognome;
    int diff=0;
    Nome=p1.getNome();
    Cognome=p1.getCognome();
    if(Cognome.equals(p2.getCognome())){
    diff=Nome.compareTo(p2.getNome());
    return diff;}
    else if(!Cognome.equals(p2.getCognome())){
    diff=Cognome.compareTo(p2.getCognome());	
    return diff;}
    else
    return 0;
    
    }
        
    }




