public class Elicottero extends Velivolo {
    public String motore() {
        return "Elicottero-motore";
    }

    public String toString() {
        return motore() + " " + super.motore() + " " + this.tipologia();
    }

    public static void main(String argv[]) {
        Velivolo v = new Elicottero();
        System.out.println(v.toString());
    }
}