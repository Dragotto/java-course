
public class Autore {
private int annoNascita;
private String nome;
public Autore(String nome, int annoNascita) {
this.nome = nome;
this.annoNascita = annoNascita;
}
public String getNome() {
return this.nome;
}
public int getAnnoNascita() {
return this.annoNascita;}

public boolean equals(Object o) {
	Autore file = (Autore)o;
	return this.getNome().equals(file.getNome());
}

public int HashCode(){
	return this.nome.hashCode();}

}
