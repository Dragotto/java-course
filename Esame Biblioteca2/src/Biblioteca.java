
import java.util.*;
public class Biblioteca {
private Map<String, Libro> codice2libro;
public Biblioteca() {
this.codice2libro = new HashMap<String, Libro>();
}
public void addLibro(String codice, Libro libro) {
this.codice2libro.put(codice, libro);
}
public List<Autore> seleziona(Selezionatore selezionatore){
List<Libro> libriInBiblioteca = new ArrayList<Libro>();
libriInBiblioteca.addAll(codice2libro.values());
List<Autore> ritorna=selezionatore.eseguiSelezione(libriInBiblioteca);
return ritorna;
}


public Map<Autore, Set<Libro>> autore2libri() {
Map<Autore, Set<Libro>> autore2libri = new HashMap<Autore, Set<Libro>>();
Set<Libro> libri=new HashSet<Libro>();
Set<Autore> autori=new HashSet<Autore>();
for(Libro l:codice2libro.values()){
 autori=l.getAutori();
 for(Autore a:autori){
	 libri.add(l);
	 autore2libri.put(a,libri); 
 }
}
return autore2libri;
}
// altri metodi omessi
}