import org.junit.*;
import java.util.*;

public class BibliotecaTest {
	private Biblioteca b;
	private Libro a,e,c,d;
	private Autore v,v1,v2,v3;
	
	
	
	@Before
	public void setUp(){
	b=new Biblioteca();
	a=new Libro("Fumo le canne");
	e=new Libro("Vado in panne");
	c=new Libro("Troppe jolle");
	d=new Libro("Divento un folle");
	v=new Autore("pusher",3);
	v1=new Autore("pushar",7);
	v2=new Autore("puher",10);
	v3=new Autore("puser",3434);
	b.addLibro(a.getTitolo(),a);
	b.addLibro(e.getTitolo(),e);
	b.addLibro(c.getTitolo(),c);
	b.addLibro(d.getTitolo(),d);
	a.addAutore(v);
	a.addAutore(v1);
	a.addAutore(v2);
	a.addAutore(v3);}
	
	@Test
	public void AutoriTest(){
	Map<Autore,Set<Libro>>  auto=new HashMap <Autore,Set<Libro>>();	
	auto=b.autore2libri();
	AssertEquals(auto.containsValue(a),true);
	AssertEquals(auto.containsValue(e),true);
	AssertEquals(auto.containsValue(c),true);
	AssertEquals(auto.containsValue(d),true);
	AssertEquals(auto.containsKey(v),true);
	AssertEquals(auto.containsKey(v1),true);
	AssertEquals(auto.containsKey(v2),true);
	AssertEquals(auto.keySet().size(),4);
	AssertEquals(auto.values().size(),4);
	}
}
