import java.util.*;
public class SelezionatoreAutoriGiovani implements Selezionatore {
	public List<Autore> eseguiSelezione(List<Libro> libriInBiblioteca){
	int annomax=0;
	List<Autore> giovani=new LinkedList<Autore>();
	Set<Autore> tutti=new HashSet<Autore>();
	
	//Cerco anno max
	for(Libro l:libriInBiblioteca){
	tutti=l.getAutori();
	 for(Autore a:tutti){
		 if(a.getAnnoNascita()>annomax){
	      annomax=a.getAnnoNascita();}}}
	//Riciclo e metto nella lista tutti autori di quell anno
	for(Libro l:libriInBiblioteca){
	tutti=l.getAutori();
	for(Autore a:tutti){
		if(a.getAnnoNascita()==annomax){
			giovani.add(a);}}}
	return giovani;}




}
