import java.util.*;
public class SelezionatoreAutoriProlifici implements Selezionatore {
	public List<Autore> eseguiSelezione(List<Libro> libriInBiblioteca){
	int nummaxlibri=0;
	int temp=0;
	Map<Autore,List<Libro>> autori2libri= new HashMap <Autore,List<Libro>>();
	Set<Autore> autori=new HashSet<Autore>();
	List<Libro> libri=new LinkedList<Libro>();
	List<Autore> prolifici=new LinkedList<Autore>();
	
	//Creo la mappa autori-Libri
	for(Libro l:libriInBiblioteca){
		autori=l.getAutori();
		for(Autore a:autori){
			libri.add(l);
			autori2libri.put(a,libri);	
		}
		//VEDO IL NUM PIU ALTO DI LIBRI
		for(Autore a:autori2libri.keySet()){
			temp=autori2libri.get(a).size();
			if(temp>nummaxlibri){
				nummaxlibri=temp;}}}
	
	//METTO NELLA LISTA TUTTI GLI AUTORI CON QUEL NUM DI LIBRI
	for(Autore a:autori2libri.keySet()){
		temp=autori2libri.get(a).size();
		if(temp==nummaxlibri){
		prolifici.add(a);}}
	return prolifici;
	}

}
